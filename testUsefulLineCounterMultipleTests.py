#! /usr/bin/env python3

import unittest
import usefulLinesCounter

class TestUsefulLineCounter(unittest.TestCase):

	def test_0_lines(self):
		self.assertEqual(usefulLinesCounter.countUsefulLines('data/file0Lines.txt'), 0, "Should be 0")

	def test_1_line_non_empty(self):
		self.assertEqual(usefulLinesCounter.countUsefulLines('data/file1LineNonEmpty.txt'), 1, "Should be 1")

	def test_1_line_space(self):
		self.assertEqual(usefulLinesCounter.countUsefulLines('data/file1LineSpace.txt'), 0, "Should be 0")


if __name__ == '__main__':
    unittest.main()
