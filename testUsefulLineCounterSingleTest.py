#! /usr/bin/env python3

import os
import glob

import unittest
import usefulLinesCounter


dataDir = "data"
solutionDir = "solution"

class TestUsefulLineCounterSingleTest(unittest.TestCase):

	def test_all_files(self):
		# manually created:
		#self.assertEqual(usefulLinesCounter.countUsefulLines('data/file0Lines.txt'), 0, "Should be 0")
		#self.assertEqual(usefulLinesCounter.countUsefulLines('data/file1LineNonEmpty.txt'), 1, "Should be 1")
		#self.assertEqual(usefulLinesCounter.countUsefulLines('data/file1LineSpace.txt'), 0, "Should be 0")

		# code for perfoming the same thing automatically
		usecaseFilePaths = glob.glob(dataDir + os.path.sep + "*.txt")

		for currentUsecaseFilePath in usecaseFilePaths:
			solutionFilePath = currentUsecaseFilePath.replace(dataDir, solutionDir)
			if os.path.exists(solutionFilePath):
				with open(solutionFilePath) as solutionFile:
					currentSolution = int(solutionFile.read().strip())
				self.assertEqual(usefulLinesCounter.countUsefulLines(currentUsecaseFilePath), currentSolution, "Should be " + str(currentSolution))


if __name__ == '__main__':
    unittest.main()
