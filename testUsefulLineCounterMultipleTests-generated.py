#! /usr/bin/env python3

import unittest
import usefulLinesCounter

class TestUsefulLineCounter(unittest.TestCase):


	def test_file0Lines(self):
		self.assertEqual(usefulLinesCounter.countUsefulLines('data/file0Lines.txt'), 0, "Should be 0")

	def test_file1LineNonEmpty(self):
		self.assertEqual(usefulLinesCounter.countUsefulLines('data/file1LineNonEmpty.txt'), 5, "Should be 5")

	def test_file1LineSpace(self):
		self.assertEqual(usefulLinesCounter.countUsefulLines('data/file1LineSpace.txt'), 0, "Should be 0")



if __name__ == '__main__':
    unittest.main()
