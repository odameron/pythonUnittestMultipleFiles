#! /usr/bin/env python3

def countUsefulLines(filePath):
	"""Returns the number of useful lines in the file filePath ."""
	nbLines = 0
	with open(filePath) as f:
		for currentLine in f:
			currentLine = currentLine.strip()
			commentStartPos = currentLine.find("#")
			if commentStartPos != -1:
				currentLine = currentLine[:commentStartPos]
			if len(currentLine) > 0:
				nbLines += 1
	return nbLines

#print(countUsefulLines('data/file0Lines.txt'))
#print(countUsefulLines('data/file1LineNonEmpty.txt'))
#print(countUsefulLines('data/file1LineSpace.txt'))

