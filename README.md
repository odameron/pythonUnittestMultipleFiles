# python Unit Test on Multiple Files

A short demo on using python unittest on a simple function processing data from a file. The unit test examines several files.


# Principles

- the file `usefulLinesCounter.py` contains the function `countUsefulLines(filePath)` that returns the number of useful lines in the file `filePath`.
    - comments are allowed. They start at the character `#` (included) until the end of the line.
    - an useful line is a line that is not empty, i.e. it contains anything other than spaces, tabulations or comments before the end of line.
- of course, we want to test `countUsefulLines(...)` on several files. For each file, we need to indicate how many useful lines it actually contains.

All the code below is based on the [unittest](https://docs.python.org/3/library/unittest.html) framework but can easily be adapted to other frameworks. 
The unit tests are stored in a separated file (eg. `testUsefulLineCounterMultipleTests.py`).
The unit tests are performed by executing `testUsefulLineCounterMultipleTests.py`.
Each successful test is represented by a point (here three successful unit tests):

    > ./testUsefulLineCounterMultipleTests.py
    ...
    ----------------------------------------------------------------------
    Ran 3 tests in 0.000s
    
    OK

Each failed test is represented by an F and you get a hint of what went wrong (here two successful and one failed unit test for which I pretended that the correct answer for `file1LineNonEmpty` should have been 5):

    > ./testUsefulLineCounterMultipleTests-generated.py
    .F.
    ======================================================================
    FAIL: test_file1LineNonEmpty (__main__.TestUsefulLineCounter)
    ----------------------------------------------------------------------
    Traceback (most recent call last):
      File "./testUsefulLineCounterMultipleTests.py", line 13, in test_file1LineNonEmpty
        self.assertEqual(usefulLinesCounter.countUsefulLines('data/file1LineNonEmpty.txt'), 5, "Should be 5")
    AssertionError: 1 != 5 : Should be 5
    
    ----------------------------------------------------------------------
    Ran 3 tests in 0.000s
    
    FAILED (failures=1)



# Approaches

There are several approaches for performing the unit tests.
The first approach should be used by default.
When there are many data file to test, generating the unit tests can be tedious.
The second approach tries to overcome this aspect by having one function that performs one single test on all the files of a directory, but this does not take advantage of the unit tests philosophy.
The third approach reconciles both aspects by having a function that automatically generates the code for individual unit tests following the first approach.

- write a function calling each file and checking the expected result (cf `testUsefulLineCounterMultipleTests.py`)
    - (+) pretty easy
	- (~) one unit test for each file
		- (+) you know which unit test suceeds or fails
		- (-) tedious to write
    - (-) requires to modify the unit test file for each new file
	- **usage:** run `testUsefulLineCounterMultipleTests.py`
- write a single generic function that tests all the files in a directory (cf `testUsefulLineCounterSingleTest.py`)
	- (~) slightly more difficult than the previous approach
	- (~) one single unit test for all the files
		- (+) new files can easily be added
		- (-) more difficult to know which or how many tests failed
	- (+) new files can easily be added
		- (-) the correct answer must also be stored somewhere
- write a program that generates an unit test for each file (cf `generateUnitTests.py`)
	- (~) slightly more difficult than the first approach
	- (+) one unit test for each file
		- (+) you know which unit test suceeds or fails
		- (+) tedious to write, but the program does it for you
	- (+) adding new test files is easy
	- usage:
		- for adding new test cases
			1. put the files to be tested in the `data` directory
			2. put the number of useful lines in a file with the same name in the `solution` directory
			3. run `generateUnitTests.py`, this will create the `testUsefulLineCounterMultipleTests-generated.py` file
		- for running the unit tests : run `testUsefulLineCounterMultipleTests-generated.py`
		
